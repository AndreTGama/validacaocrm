<?php require('server.php'); ?>
<html>
<head>
    <title>Confimação</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="script" href="server.php">
</head>
<body>
<!-- Inicio do formulario -->
<form method="get" action="server.php">
    <fieldset disabled>
        <div class="form-group">
            <label for="exampleInputEmail1">CRM</label>
            <input value="<?=$CRM?>" type="text" class="form-control" id="exampleInputEmail1" name="CRM">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Estado</label>
            <input value="<?=$UF?>" type="text" class="form-control" id="exampleInputPassword1" name="UF">
        </div>
        <div class="form-group">
            <label for="exampleInputName">Nome</label>
            <input value="<?=$nome?>" type="text" class="form-control" id="exampleInputName" name="NOME">
        </div>
        <div class="form-group">
            <label for="exampleInputSituacao">Situação</label>
            <input value="<?=$status?>" type="text" class="form-control" id="exampleInputSituacao" name="SITUACAO">
        </div>
        <div class="form-group">
            <label for="exampleInputSituacao">Profissão</label>
            <input value="<?=$profi?>" type="text" class="form-control" id="exampleInputSituacao" name="PROFISSAO">
        </div>
    </fieldset>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>